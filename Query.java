import java.util.Properties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.io.FileInputStream;

/**
 * Runs queries against a back-end database
 */
public class Query {
    private static Properties configProps = new Properties();

    private static String imdbUrl;
    private static String customerUrl;

    private static String postgreSQLDriver;
    private static String postgreSQLUser;
    private static String postgreSQLPassword;

    private static String current_user;
    private static String current_user_plan;//added by Santiago
    private static String current_user_name;//added by Santiago

    // DB Connection
    private Connection _imdb;
    private Connection _customer_db;

    // Canned queries

    private String _search_sql = "SELECT * FROM movie WHERE name like ? ORDER BY id";
    private PreparedStatement _search_statement;

    private String _director_mid_sql = "SELECT y.* "
                     + "FROM movie_directors x, directors y "
                     + "WHERE x.mid = ? and x.did = y.id";
    private PreparedStatement _director_mid_statement;
    // adding query statement for part B
    private String _actor_mid_sql = "SELECT actor.* "
                     + "FROM actor, casts "
                     + "WHERE actor.id = casts.pid and casts.mid = ?";
    private PreparedStatement _actor_mid_statement;
    private String _rental_mid_sql = "SELECT cid "
                     + "FROM rentals "
                     + "WHERE mid = ? AND open = TRUE";
    private PreparedStatement _rental_mid_statement;

    /* uncomment, and edit, after your create your own customer database */
    
    private String _customer_login_sql = "SELECT * FROM customers WHERE username = ? and password = ?";
    private PreparedStatement _customer_login_statement;

    private String _customer_name_sql = "SELECT * FROM customers WHERE id = ?";
    private PreparedStatement _customer_name_statement;

    private String _begin_transaction_read_write_sql = "BEGIN TRANSACTION READ WRITE";
    private PreparedStatement _begin_transaction_read_write_statement;

    private String _commit_transaction_sql = "COMMIT TRANSACTION";
    private PreparedStatement _commit_transaction_statement;

    private String _rollback_transaction_sql = "ROLLBACK TRANSACTION";
    private PreparedStatement _rollback_transaction_statement;

    private String f_act_sql = "SELECT c.mid, a.fname, a.lname " 
                                + "FROM movie m, actor a, casts c " 
                                + "WHERE m.name like ? AND m.id = c.mid AND c.pid = a.id " 
                                + "ORDER BY m.id"; 
    private PreparedStatement f_act_statement;

    private String f_dir_sql = "SELECT md.mid, d.fname, d.lname " 
                                + "FROM movie m, directors d, movie_directors md " 
                                + "WHERE m.name like ? AND m.id = md.mid AND md.did = d.id " 
                                + "ORDER BY md.mid"; 
    private PreparedStatement f_dir_statement;

    private String _choose_plan_sql = "UPDATE customers SET plan_id = ? WHERE id = ?";
    private PreparedStatement _choose_plan_statement;

    private String _customer_get_plan_sql = "SELECT plan_id FROM customers WHERE id = ?";
    private PreparedStatement _customer_get_plan_statement;
    
    private String _list_plans_sql = "SELECT * FROM plans";
    private PreparedStatement _list_plans_statement;
    
    private String _find_plan_sql = "SELECT * FROM plans WHERE id = ?";
    private PreparedStatement _find_plan_statement;

    private String _count_customer_rentals_sql = "SELECT COUNT(*) FROM rentals WHERE cid = ? AND open = TRUE";
    private PreparedStatement _count_customer_rentals_statement;

    private String customer_rented_movies_sql = "SELECT m.name FROM movie m, rentals r WHERE m.id = r.mid AND r.cid = ? AND r.open = TRUE";
    private PreparedStatement customer_rented_movies_statement;

    private String _count_plan_max_rentals_sql = "SELECT max_rentals FROM plans WHERE id = ?";
    private PreparedStatement _count_plan_max_rentals_statement;

    private String _movie_exists_sql = "SELECT * from movie where id = ?";
    private PreparedStatement _movie_exists_statement;

    private String _set_movie_rented_sql = "INSERT INTO rentals(mid, cid, open) VALUES (?, ?, TRUE)";
    private PreparedStatement _set_movie_rented_statement;

    private String _return_movie_sql = "UPDATE rentals set open = FALSE WHERE cid = ? AND mid = ?";
    private PreparedStatement _return_movie_statement;

    public Query() {
    }

    /**********************************************************/
    /* Connections to postgres databases */

    public void openConnection() throws Exception {
        configProps.load(new FileInputStream("dbconn.config"));
        
        
        imdbUrl        = configProps.getProperty("imdbUrl");
        customerUrl    = configProps.getProperty("customerUrl");
        postgreSQLDriver   = configProps.getProperty("postgreSQLDriver");
        postgreSQLUser     = configProps.getProperty("postgreSQLUser");
        postgreSQLPassword = configProps.getProperty("postgreSQLPassword");


        /* load jdbc drivers */
        Class.forName(postgreSQLDriver).newInstance();

        /* open connections to TWO databases: imdb and the customer database */
        _imdb = DriverManager.getConnection(imdbUrl, // database
                postgreSQLUser, // user
                postgreSQLPassword); // password

        _customer_db = DriverManager.getConnection(customerUrl, // database
                postgreSQLUser, // user
                postgreSQLPassword); // password
    }

    public void closeConnection() throws Exception {
        _imdb.close();
        _customer_db.close();
    }

    /**********************************************************/
    /* prepare all the SQL statements in this method.
      "preparing" a statement is almost like compiling it.  Note
       that the parameters (with ?) are still not filled in */

    public void prepareStatements() throws Exception {

        _search_statement = _imdb.prepareStatement(_search_sql);
        _director_mid_statement = _imdb.prepareStatement(_director_mid_sql);
        

        /* uncomment after you create your customers database */
        
        _customer_login_statement = _customer_db.prepareStatement(_customer_login_sql);
        _customer_name_statement = _customer_db.prepareStatement(_customer_name_sql);//added
        _begin_transaction_read_write_statement = _customer_db.prepareStatement(_begin_transaction_read_write_sql);
        _commit_transaction_statement = _customer_db.prepareStatement(_commit_transaction_sql);
        _rollback_transaction_statement = _customer_db.prepareStatement(_rollback_transaction_sql);
        customer_rented_movies_statement = _customer_db.prepareStatement(customer_rented_movies_sql);


        /* add here more prepare statements for all the other queries you need */
        _actor_mid_statement = _imdb.prepareStatement(_actor_mid_sql);
        f_act_statement = _imdb.prepareStatement(f_act_sql);
        f_dir_statement = _imdb.prepareStatement(f_dir_sql);
        _movie_exists_statement = _imdb.prepareStatement(_movie_exists_sql);
        _rental_mid_statement = _customer_db.prepareStatement(_rental_mid_sql);
        _choose_plan_statement = _customer_db.prepareStatement(_choose_plan_sql);
        _list_plans_statement = _customer_db.prepareStatement(_list_plans_sql);
        _find_plan_statement = _customer_db.prepareStatement(_find_plan_sql);
        _count_customer_rentals_statement = _customer_db.prepareStatement(_count_customer_rentals_sql);
        _count_plan_max_rentals_statement = _customer_db.prepareStatement(_count_plan_max_rentals_sql);
	   _customer_get_plan_statement = _customer_db.prepareStatement(_customer_get_plan_sql);
	   _set_movie_rented_statement = _customer_db.prepareStatement(_set_movie_rented_sql);
       _return_movie_statement = _customer_db.prepareStatement(_return_movie_sql);
    }


    /**********************************************************/
    /* suggested helper functions  */

    public int helper_compute_remaining_rentals(int cid) throws Exception {
        /* how many movies can she/he still rent ? */
        /* you have to compute and return the difference between the customer's plan
           and the count of oustanding rentals */
	    _count_customer_rentals_statement.clearParameters();
	    _count_customer_rentals_statement.setInt(1, cid);
	    ResultSet count_rentals = _count_customer_rentals_statement.executeQuery();
	    _customer_get_plan_statement.clearParameters();
	    _customer_get_plan_statement.setInt(1, cid);
	    ResultSet plan_num = _customer_get_plan_statement.executeQuery();
	    _count_plan_max_rentals_statement.clearParameters();
        plan_num.next();
	    _count_plan_max_rentals_statement.setInt(1, plan_num.getInt(1));
	    ResultSet num_rentals = _count_plan_max_rentals_statement.executeQuery();
        num_rentals.next();
        count_rentals.next();
        return (num_rentals.getInt(1)-count_rentals.getInt(1));
    }

    public String helper_compute_customer_name(int cid) throws Exception {
        /* you find  the first + last name of the current customer */
        _customer_name_statement.clearParameters();
        _customer_name_statement.setInt(1, cid);
        ResultSet name_set = _customer_name_statement.executeQuery();

        name_set.next();
        return (name_set.getString("fname") + " " +name_set.getString("lname") );
    }

    public String helper_plan_type(int plan_id) throws Exception {
        //return the type / name of a plan given the id
        _find_plan_statement.clearParameters();
        _find_plan_statement.setInt(1, plan_id);
        ResultSet plan_set = _find_plan_statement.executeQuery();
        plan_set.next();
        return plan_set.getString(2);
    }

    public boolean helper_check_plan(int plan_id) throws Exception {
        /* is plan_id a valid plan id ?  you have to figure out */
        _find_plan_statement.setInt(1, plan_id);
        ResultSet plan_exists_set = _find_plan_statement.executeQuery();
        if (!plan_exists_set.next()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean helper_check_movie(int mid) throws Exception {
        /* is mid a valid movie id ? you have to figure out  */
        _movie_exists_statement.clearParameters();
        _movie_exists_statement.setInt(1, mid);
        ResultSet movie_exists_set = _movie_exists_statement.executeQuery();
        if (movie_exists_set.next())
            return true;
        else
            return false;
    }

    private int helper_who_has_this_movie(int mid) throws Exception {
        /* find the customer id (cid) of whoever currently rents the movie mid; return -1 if none */
        _rental_mid_statement.clearParameters();
        _rental_mid_statement.setInt(1, mid);
        ResultSet rental_set = _rental_mid_statement.executeQuery();
        if (rental_set.next()){
            int result = rental_set.getInt(1);
            rental_set.close();
            return result;
        } else {
            rental_set.close();
            return -1;
        }
    }

    /**********************************************************/
    /* login transaction: invoked only once, when the app is started  */
    public int transaction_login(String name, String password) throws Exception {
        /* authenticates the user, and returns the user id, or -1 if authentication fails */

        /* Uncomment after you create your own customers database */
        
        int cid;
        current_user = name;
        _customer_login_statement.clearParameters();
        _customer_login_statement.setString(1,name);
        _customer_login_statement.setString(2,password);
        ResultSet cid_set = _customer_login_statement.executeQuery();
        if (cid_set.next()){ 
            cid = cid_set.getInt(1);
            //cache the current user's plan type and username
            int plan_id = cid_set.getInt(3);
            current_user_plan = helper_plan_type(plan_id); // current user plan
            current_user_name = cid_set.getString(5);// current user name
        }
       else cid = -1;
        return(cid);
         
    }

    public void transaction_personal_data(int cid) throws Exception {
        /* println the customer's personal data: name, and plan number */
         System.out.println("Name: " + helper_compute_customer_name(cid) +" Plan: "+ current_user_plan + 
                            " Remaining Rentals: "+helper_compute_remaining_rentals(cid));// added by santiago
    }


    /**********************************************************/
    /* main functions in this project: */

    public void transaction_search(int cid, String movie_title)
            throws Exception {
        /* searches for movies with matching titles: SELECT * FROM movie WHERE name LIKE movie_title */
        /* prints the movies, directors, actors, and the availability status:
           AVAILABLE, or UNAVAILABLE, or YOU CURRENTLY RENT IT */

        /* set the first (and single) '?' parameter */
        _search_statement.clearParameters();
        _search_statement.setString(1, '%' + movie_title + '%');

        ResultSet movie_set = _search_statement.executeQuery();
        while (movie_set.next()) {
            int mid = movie_set.getInt(1);
            System.out.println("ID: " + mid + " NAME: "
                    + movie_set.getString(2) + " YEAR: "
                    + movie_set.getString(3));
            /* do a dependent join with directors */
            _director_mid_statement.clearParameters();
            _director_mid_statement.setInt(1, mid);
            ResultSet director_set = _director_mid_statement.executeQuery();
            while (director_set.next()) {
                System.out.println("\t\tDirector: " + director_set.getString(3)
                        + " " + director_set.getString(2));
            }
            director_set.close();
            // now you need to retrieve the actors, in the same manner 
            // do a dependent join for actor
            _actor_mid_statement.clearParameters();
            _actor_mid_statement.setInt(1, mid);
            ResultSet actor_set = _actor_mid_statement.executeQuery();
            while (actor_set.next()) {
                System.out.println("\t\tActor: " + actor_set.getString(3)
                        + " " + actor_set.getString(2));
            }
            actor_set.close();
            /* then you have to find the status: of "AVAILABLE" "YOU HAVE IT", "UNAVAILABLE" */
            int rental_check = helper_who_has_this_movie(mid);
            if (rental_check != -1){
                // check to see if the one rent this movie has the same cid with the current customer
                if (rental_check == cid){
                    System.out.println("YOU HAVE IT");
                } else {
                    System.out.println("UNAVAILABLE");
                }
            } else {
                System.out.println("AVAILABLE");
            }
        }
        System.out.println();
    }

    public void transaction_choose_plan(int cid, int pid) throws Exception {
        /* updates the customer's plan to pid: UPDATE customers SET plid = pid */
        /* remember to enforce consistency ! */
        _begin_transaction_read_write_statement.execute();
        //Check if new plan has fewer max rentals than customer's current rentals
        //get number of movies customer has out
        _count_customer_rentals_statement.clearParameters();
        _count_customer_rentals_statement.setInt(1, cid);
        ResultSet customer_count_set = _count_customer_rentals_statement.executeQuery();
        customer_count_set.next();
        int customer_rentals = customer_count_set.getInt(1);
        _count_plan_max_rentals_statement.clearParameters();
        _count_plan_max_rentals_statement.setInt(1, pid);
        //get plan's max rentals
        ResultSet plan_max_set = _count_plan_max_rentals_statement.executeQuery();
        plan_max_set.next();
        int plan_rentals = plan_max_set.getInt(1);
        //compare, abort if current rentals > plan max
        if (customer_rentals > plan_rentals) {
            _rollback_transaction_statement.execute();
            System.out.println("You cannot switch to a plan that doesn't allow as many rentals as you have out");
            return;
        }
        //If we got this far, can go ahead and set the plan
        _choose_plan_statement.setInt(1, pid);
        _choose_plan_statement.setInt(2, cid);
        _choose_plan_statement.executeUpdate();
        _commit_transaction_statement.execute();
        //cache the current user's plan type
        current_user_plan = helper_plan_type(pid);
    }

    public void transaction_list_plans() throws Exception {
        /* println all available plans: SELECT * FROM plan */
        ResultSet plan_set = _list_plans_statement.executeQuery();
        while (plan_set.next()) {
            System.out.println(plan_set.getInt(1)+"|"+plan_set.getString(2)+"|Monthly Rentals: "+plan_set.getInt(3)+"|Fee: "+plan_set.getInt(4));
        }
    }
    
    public void transaction_list_user_rentals(int cid) throws Exception {
        /* println all movies rented by the current user*/
	 customer_rented_movies_statement.clearParameters();
        customer_rented_movies_statement.setInt(1, cid);
        ResultSet rent_set = customer_rented_movies_statement.executeQuery();

        rent_set.next();//to eliminate duplicate
        while(rent_set.next())
            System.out.println(rent_set.getString("name"));
    }

    public void transaction_rent(int cid, int mid) throws Exception {
        _begin_transaction_read_write_statement.execute();
	    if(helper_compute_remaining_rentals(cid) > 0){
	   	   int rental_check = helper_who_has_this_movie(mid);
            if (rental_check == -1){
                if (helper_check_movie(mid) == false) {
                    _rollback_transaction_statement.execute();
                    System.out.println("No movie exists with that id");
                    return;
                }
                _set_movie_rented_statement.clearParameters();
                _set_movie_rented_statement.setInt(1, mid);
                _set_movie_rented_statement.setInt(2, cid);
                _set_movie_rented_statement.executeUpdate();
                _commit_transaction_statement.execute();
            }
            else{
                _rollback_transaction_statement.execute();
                if (rental_check == cid)
                    System.out.println("You already have it!");
                else
                    System.out.println("Someone already has it!");
            }
	    }
	    else{
            _rollback_transaction_statement.execute();
		    System.out.println("You have no rentals remaining!");
	    }
    }

		

    public void transaction_return(int cid, int mid) throws Exception {
        /* return the movie mid by the customer cid */
        _begin_transaction_read_write_statement.execute();
        if (helper_who_has_this_movie(mid) != cid) {
            _rollback_transaction_statement.execute();
            System.out.println("You aren't renting this movie");
            return;
        }
        _return_movie_statement.clearParameters();
        _return_movie_statement.setInt(1, cid);
        _return_movie_statement.setInt(2, mid);
        _return_movie_statement.executeUpdate();
        _commit_transaction_statement.execute();
        return;
    }

    public void transaction_fast_search(int cid, String movie_title)
            throws Exception {
        /* like transaction_search, but uses joins instead of independent joins
           Needs to run three SQL queries: (a) movies, (b) movies join directors, (c) movies join actors
           Answers are sorted by mid.
           Then merge-joins the three answer sets */
        //Get movies ordered by id
        String search_string = '%' + movie_title + '%';
        _search_statement.clearParameters(); 
        _search_statement.setString(1, search_string); 
        ResultSet f_mov_set = _search_statement.executeQuery();
        //Get directors ordered by movie id
        f_dir_statement.clearParameters(); 
        f_dir_statement.setString(1, search_string); 
        ResultSet f_dir_set = f_dir_statement.executeQuery();
        //get actors ordered by movie id
        f_act_statement.clearParameters(); 
        f_act_statement.setString(1, search_string);
        ResultSet f_act_set = f_act_statement.executeQuery();
        //initialize the ResultSets
        boolean got_directors = f_dir_set.next();
        boolean got_actors = f_act_set.next();
        //iterate over the movies
        while (f_mov_set.next()) {
            int movie_id = f_mov_set.getInt(1);
            System.out.println("ID: " + movie_id + " NAME: " + f_mov_set.getString(2) + " YEAR: " + f_mov_set.getString(3));
            //iterate over all directors with the same movie id as current movie
            while (got_directors && f_dir_set.getInt(1)==movie_id) {
                System.out.println("\t\tDirector: " + f_dir_set.getString(3) + " " + f_dir_set.getString(2));
                got_directors = f_dir_set.next();
            }
            //iterate over all actors with the same movie id as current movie
            while (got_actors && f_act_set.getInt(1)==movie_id) {
                System.out.println("\t\tActor: " + f_act_set.getString(3) + " " + f_act_set.getString(2));
                got_actors = f_act_set.next();
            }
                         
        }
    }
}
