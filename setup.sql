CREATE TABLE Plans (id serial PRIMARY KEY, type varchar(20), max_rentals int, fee int);

CREATE TABLE Customers (id int, username varchar(20) UNIQUE, plan_id int, password varchar(20), fname varchar(30), lname varchar(30), address varchar(50), city varchar(20), PRIMARY KEY (id), FOREIGN KEY (plan_id) REFERENCES Plans);

CREATE TABLE Rentals (id serial, mid int NOT NULL, cid int, open boolean, PRIMARY KEY (id), FOREIGN KEY (cid) REFERENCES Customers);

INSERT INTO Plans(type, max_rentals, fee) VALUES ('Basic', 5, 10), ('Value', 2, 5), ('Deluxe', 10, 15);

INSERT INTO Customers VALUES (1, 'johnd39', 1, 'johnmovies', 'John', 'Doe', '39 Main Street', 'Hanover'), (2, 'richfield63', 3, 'richmovies', 'Richard', 'Garfield', '74 Arborway', 'Boston');

INSERT INTO Rentals(mid, cid, open) VALUES (1517926, 2, FALSE), (53, 2, FALSE), (65, 2, TRUE), (67, 1, TRUE), (58, 1, FALSE), (58, 1, TRUE);

